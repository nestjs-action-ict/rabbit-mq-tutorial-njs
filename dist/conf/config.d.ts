export declare const environment: {
    SERVER_HOST: string;
    QUEUE: string;
    SERVER_ADDRESS: string;
    SERVER_PATH: string;
};
