"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.disconnect = exports.connect = void 0;
var amqp = require('amqplib/callback_api');
exports.connect = (host, connection) => {
    amqp.connect('amqp://' + host, connection);
};
exports.disconnect = (connection, channel) => {
    channel.close(function (err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log('Channel closed');
            connection.close();
            console.log('Connection closed');
        }
    });
};
//# sourceMappingURL=queue.utils.js.map