import { QueueService } from './queue.service';
export declare class QueueController {
    private readonly queueService;
    constructor(queueService: QueueService);
    start(): Promise<void>;
    stop(): Promise<void>;
    receive(): Promise<void>;
}
