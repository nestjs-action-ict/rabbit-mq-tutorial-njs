"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueueService = void 0;
const common_1 = require("@nestjs/common");
const queue_utils_1 = require("./utils/queue.utils");
const config_1 = require("../conf/config");
const axios_1 = require("axios");
let QueueService = class QueueService {
    async start() {
        const self = this;
        this.queue = process.env.QUEUE || config_1.environment.QUEUE;
        queue_utils_1.connect(process.env.SERVER_HOST || config_1.environment.SERVER_HOST, function (error0, connection) {
            if (error0) {
                throw error0;
            }
            self.connection = connection;
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    throw error1;
                }
                self.channel = channel;
                channel.assertQueue(self.queue, {
                    durable: false
                });
            });
        });
    }
    async stop() {
        queue_utils_1.disconnect(this.connection, this.channel);
    }
    async receive() {
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", this.queue);
        this.channel.consume(this.queue, function (msg) {
            console.log(" [x] Received %s", msg.content.toString());
            axios_1.default.post(`${process.env.SERVER_ADDRESS || config_1.environment.SERVER_ADDRESS}/${process.env.SERVER_PATH || config_1.environment.SERVER_PATH}`, {
                message: msg.content.toString(),
                from: 'RabbitMQ'
            }).then(() => console.log('Message delivered'), (err) => console.log('Error delivering: ', err));
        }, {
            noAck: true
        });
    }
};
QueueService = __decorate([
    common_1.Injectable()
], QueueService);
exports.QueueService = QueueService;
//# sourceMappingURL=queue.service.js.map