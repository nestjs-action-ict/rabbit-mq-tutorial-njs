export declare class QueueService {
    connection: any;
    channel: any;
    queue: string;
    start(): Promise<void>;
    stop(): Promise<void>;
    receive(): Promise<void>;
}
