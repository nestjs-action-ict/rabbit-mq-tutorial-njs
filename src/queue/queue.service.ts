import { Injectable } from '@nestjs/common';
import { connect, disconnect } from './utils/queue.utils';
import { environment } from '../conf/config';
import axios from 'axios';

@Injectable()
export class QueueService {

  connection: any;
  channel: any;
  queue: string;

  async start() {

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;
    this.queue = process.env.QUEUE || environment.QUEUE;

    connect(process.env.SERVER_HOST || environment.SERVER_HOST, function(error0, connection) {
      if (error0) {
        throw error0;
      }
      self.connection = connection;

      connection.createChannel(function(error1, channel) {
        if (error1) {
          throw error1;
        }
        self.channel = channel;

        channel.assertQueue(self.queue, {
          durable: false
        });


      });
    });
  }

  async stop() {
    disconnect(this.connection, this.channel);
  }

  async receive() {

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", this.queue);

    this.channel.consume(this.queue, function(msg) {
      console.log(" [x] Received %s", msg.content.toString());
      axios.post(`${process.env.SERVER_ADDRESS || environment.SERVER_ADDRESS}/${process.env.SERVER_PATH || environment.SERVER_PATH}`, {
        message: msg.content.toString(),
        from: 'RabbitMQ'
      }).then(() => console.log('Message delivered'), (err) => console.log('Error delivering: ', err))
    }, {
      noAck: true
    });
  }

}
