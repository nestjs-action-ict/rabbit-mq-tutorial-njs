var amqp = require('amqplib/callback_api');

export const connect = (host, connection) => {
  amqp.connect('amqp://' + host, connection);
}

export const disconnect = (connection, channel) => {
  channel.close(function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log('Channel closed');
      connection.close();
      console.log('Connection closed')
    }
  });

}
