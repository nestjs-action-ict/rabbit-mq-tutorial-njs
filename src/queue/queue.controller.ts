import { Controller, Get } from '@nestjs/common';
import { QueueService } from './queue.service';

@Controller('queue')
export class QueueController {

  constructor(private readonly queueService: QueueService) {}

  @Get('start')
  async start() {
    this.queueService.start();
  }

  @Get('stop')
  async stop() {
    this.queueService.stop();
  }

  @Get('receive')
  async receive() {
    this.queueService.receive();
  }


}
